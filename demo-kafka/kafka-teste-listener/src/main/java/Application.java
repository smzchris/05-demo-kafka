import java.time.Duration;
import java.util.Collections;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;

public class Application {

	private static final String TOPIC = "transacoes-cartao";
	private static final String SERVERS = "localhost:9092";
	
	//private static final int C_THREADS = 3;
	//private static final String GROUPS[] = new String[] {"GrupoA", "GrupoB"};
	
	
	/**
	 * 
	 * @param index
	 * @return
	 */
	public static KafkaProducer<Long, String> createProducer(int index) {
		Properties props = new Properties();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, SERVERS);
		props.put(ProducerConfig.CLIENT_ID_CONFIG, "Produtor-" + index);
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class);
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);

		KafkaProducer<Long, String> producer = new KafkaProducer<>(props);
		return producer;
	}
	
	/**
	 * 
	 * @param index
	 * @return
	 */
	public static KafkaConsumer<Long, String> createConsumer(String group, int index) {
		Properties props = new Properties();
		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, SERVERS);
		props.put(ConsumerConfig.CLIENT_ID_CONFIG, "Consumidor-" + index);
		props.put(ConsumerConfig.GROUP_ID_CONFIG, group);
		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, LongDeserializer.class);
		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);

		KafkaConsumer<Long, String> consumer = new KafkaConsumer<>(props);
		return consumer;
	}
	
	
	public static void main(String[] args) {
		//testeProducer();
		
		testeConsumer();
	}
	
	public static void testeProducer() {
		System.out.print("Teste");
		KafkaProducer<Long, String> producer = createProducer(1);
		
		Random random = new Random();
		final String topico = TOPIC;
		final long key = random.nextInt(10000000);
		final String body = "Teste da mensagem " + key;
		
		ProducerRecord<Long, String> record = new ProducerRecord<>(topico, key, body);
		
		try {
			producer.send(record).get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		producer.flush();
		producer.close();
		System.out.print("Fim");
	}
	
	public static void testeConsumer() {
		System.out.print("Teste");
		System.out.printf("Thread [%s] starting.\n", Thread.currentThread().getName());
		KafkaConsumer<Long, String> consumer = createConsumer("group", 1);
		consumer.subscribe(Collections.singletonList(TOPIC));
		
		try {
			while (true) {
				
				ConsumerRecords<Long, String> records = consumer.poll(Duration.ofMillis(500));

				for (ConsumerRecord<Long, String> record : records) {
					System.out.printf(
							" [%s] Consumed: topic=[%s] partition=[%d] offset=[%d] key=[%d] value=[%s] \n",
							Thread.currentThread().getName(),
							record.topic(),
							record.partition(),
							record.offset(),
							record.key(),
							record.value());
				}
			}
		} finally {
			consumer.close();
		}
		
	    
		
	}

}
